import java.time.LocalDateTime;
import java.util.Date;

public class  MonitoredData {
	private Date startTime;
	private Date endTime;
	private String activity;
	private String patientId;
	
	public MonitoredData(Date startTime, Date endTime, String activity,String patientId) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
		this.patientId = patientId;
	}

	public MonitoredData(){}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	@Override
	public String toString() {
		return "MonitoredData{" +
				"startTime=" + startTime +
				", endTime=" + endTime +
				", activity=" + activity +
				", patient=" + patientId +
				'}';
	}
}
