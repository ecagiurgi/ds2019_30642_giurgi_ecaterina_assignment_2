import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Producer {

    private final static String QUEUE_NAME = "activities";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = "Hello World!";
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + message + "'");
        }
    }

    public static List<MonitoredData> read() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fileName = "activity.txt";

        // read file into stream, try-with-resources
        try (Stream<String> lines = Files.lines(Paths.get(fileName))) {

            List<MonitoredData> monitoredDataList = lines.map(line -> line.split("\\t\\t")).map(strVector -> {
                String startTimeStr = strVector[0];
                Date startTime = null;
                try {
                    startTime = formatter.parse(startTimeStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String endTimeStr = strVector[1];
                Date endTime = null;
                try {
                    endTime = formatter.parse(endTimeStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String activity = strVector[2];
                activity = activity.replaceAll("\\s", "");

                String patientId = getRandomString();
                return new MonitoredData(startTime, endTime, activity,patientId);
            }).collect(Collectors.toList());
            return monitoredDataList;


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public static void toJson() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        List<MonitoredData> list = read();
        ObjectMapper obj = new ObjectMapper();
        for (MonitoredData monitoredData : list) {
            try {
                //System.out.println(monitoredData.toString());
                String json = obj.writeValueAsString(monitoredData);
                //  System.out.println(json);
                try (Connection connection = factory.newConnection();
                     Channel channel = connection.createChannel()) {
                    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                    String message = json;
                    channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
                    System.out.println(" [x] Sent '" + message + "'");
                } catch (TimeoutException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            //System.out.println(monitoredData.toString());
        }
    }

    static String getRandomString() {
        int r = (int) (Math.random() * 5);
        String patientId = new String[]{"07d17a36-144f-48e4-891d-5ef8831dffc6", "1214ef34-de86-4454-8181-2f72b49baf89", "1ecec9e4-f469-4677-a9fd-7ec29900b00a", "9a75a9d0-f834-4d31-b030-2ee547b9cb9c","b7a80f33-1a65-4991-a1b2-73dd1380b435"}[r];
        return patientId;
    }
}
