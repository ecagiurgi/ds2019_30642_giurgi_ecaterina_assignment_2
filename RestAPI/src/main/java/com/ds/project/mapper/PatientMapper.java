package com.ds.project.mapper;

import com.ds.project.dto.PatientDTO;
import com.ds.project.entity.Patient;

public interface PatientMapper {

    PatientDTO toDTO(Patient entity);
    Patient toEntity(PatientDTO dto);
    void toEntityUpdate(Patient patient, PatientDTO patientDTO);
}
