package com.ds.project.mapper.impl;

import com.ds.project.dto.DoctorDTO;
import com.ds.project.dto.UserDTO;
import com.ds.project.entity.Doctor;
import com.ds.project.entity.User;
import com.ds.project.mapper.DoctorMapper;
import com.ds.project.mapper.UserMapper;
import org.springframework.stereotype.Component;

@Component
public class DoctorMapperImpl implements DoctorMapper {
    UserMapper userMapper = new UserMapperImpl();

    @Override
    public DoctorDTO toDTO(Doctor entity) {
        if ( entity == null ) {
            return null;
        }

        DoctorDTO doctorDTO = new DoctorDTO();

        doctorDTO.setDoctorId( entity.getId() );
        doctorDTO.setName( entity.getName() );
        doctorDTO.setBirthDate( entity.getBirthDate() );
        doctorDTO.setAdress( entity.getAdress() );
        doctorDTO.setGender( entity.getGender() );
        doctorDTO.setUser1Dto(userMapper.toDTO(entity.getUser1()));

        return doctorDTO;       }

    @Override
    public Doctor toEntity(DoctorDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Doctor doctor = new Doctor();

        doctor.setId( dto.getDoctorId() );
        doctor.setName( dto.getName() );
        doctor.setBirthDate( dto.getBirthDate() );
        doctor.setAdress( dto.getAdress() );
        doctor.setGender( dto.getGender() );
        doctor.setUser1(userMapper.toEntity(dto.getUser1Dto()) );

        return doctor;       }

    @Override
    public void toEntityUpdate(Doctor doctor, DoctorDTO doctorDTO) {
        if ( doctorDTO == null ) {
            return;
        }
        doctor.setId( doctorDTO.getDoctorId() );
        doctor.setName( doctorDTO.getName() );
        doctor.setBirthDate( doctorDTO.getBirthDate() );
        doctor.setAdress( doctorDTO.getAdress() );
        doctor.setGender( doctorDTO.getGender() );
        doctor.setUser1(userMapper.toEntity(doctorDTO.getUser1Dto()) );

    }


}
