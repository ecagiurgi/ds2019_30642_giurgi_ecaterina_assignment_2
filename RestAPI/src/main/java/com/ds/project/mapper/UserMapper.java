package com.ds.project.mapper;
import com.ds.project.dto.UserDTO;
import com.ds.project.entity.User;
//    @Autowired
//    EntityManager entityManager;
//
//    @Mapping(target = "username", source = "username")
//    public abstract User toEntity(UserDTO userDTO);
//
//    @Mapping(target = "username", source = "username")
//    public abstract void toEntityUpdate(@MappingTarget User user, UserDTO userDTO);
//
//    @Mapping(target = "username", source = "username")
//    public abstract UserDTO toDTO(User user);
//
////    public User toEntity(NameIdDTO nameIdDTO) {
////        return entityManager.find(User.class, nameIdDTO.getId());
////    }
//}
public interface UserMapper {

    UserDTO toDTO(User entity);
    User toEntity(UserDTO dto);
    void toEntityUpdate(User user, UserDTO userDTO);

}