package com.ds.project.mapper;

import com.ds.project.dto.SideEffectDTO;
import com.ds.project.entity.SideEffect;

public interface SideEffectMapper {

    SideEffectDTO toDTO(SideEffect entity);
    SideEffect toEntity(SideEffectDTO dto);
    void toEntityUpdate(SideEffect sideEffect, SideEffectDTO sideEffectDTO);
}
