package com.ds.project.mapper;

import com.ds.project.dto.IntakeDTO;
import com.ds.project.entity.Intake;

public interface IntakeMapper {
    IntakeDTO toDTO(Intake entity);
    Intake toEntity(IntakeDTO dto);
    void toEntityUpdate(Intake intake, IntakeDTO intakeDTO);
}
