package com.ds.project.repository;

import com.ds.project.entity.Intake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntakeRepository  extends JpaRepository<Intake, Long> {
    Intake findByIntakeId(final Long  id);
}
