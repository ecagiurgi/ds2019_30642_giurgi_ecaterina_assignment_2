package com.ds.project.repository;

import com.ds.project.entity.SideEffect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SideEffectsRepository  extends JpaRepository<SideEffect, Long> {
    SideEffect findBySideEffectId(final Long  id);
}
