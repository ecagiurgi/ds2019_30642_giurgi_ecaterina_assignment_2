package com.ds.project.util;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateConverter {

    public static LocalDate convertTimeStampToLocalDate(Timestamp time){
        return time.toLocalDateTime().toLocalDate();
    }

    public static Timestamp convertLocalDateToTimeStamp(LocalDate date){
        return Timestamp.valueOf(date.atStartOfDay());
    }

}