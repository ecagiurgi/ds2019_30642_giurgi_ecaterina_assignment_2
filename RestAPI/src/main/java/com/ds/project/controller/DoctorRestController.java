package com.ds.project.controller;

import com.ds.project.dto.DoctorDTO;
import com.ds.project.service.DoctorSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/doctor")
public class DoctorRestController {

    private final DoctorSerivce doctorService;

    @Autowired
    public DoctorRestController(DoctorSerivce doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/list")
    public List<DoctorDTO> list() {
        return doctorService.findAll();
    }

    @GetMapping("/{id}")
    public DoctorDTO findById(@PathVariable("id") Long id) {
        return doctorService.findById(id);
    }

    @PostMapping("/save")
    public DoctorDTO save(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.save(doctorDTO);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        doctorService.deleteDoctor(id);
    }


}
