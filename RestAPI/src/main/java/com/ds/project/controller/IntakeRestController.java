package com.ds.project.controller;

import com.ds.project.dto.IntakeDTO;
import com.ds.project.service.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/intake")
public class IntakeRestController {
    private final IntakeService intakeService;

    @Autowired
    public IntakeRestController(IntakeService intakeService)
    {
        this.intakeService = intakeService;
    }

    @GetMapping("/list")
    public List<IntakeDTO> list() {
        return intakeService.findAll();
    }

    @GetMapping("/{id}")
    public IntakeDTO findById(@PathVariable("id") Long id) {
        return intakeService.findById(id);
    }

    @PostMapping("/save")
    public IntakeDTO save(@RequestBody IntakeDTO intakeDTO) {
        return intakeService.save(intakeDTO);
    }

    @GetMapping("/delete/{id}")
    public IntakeDTO delete(@PathVariable("id") Long id) {
        return intakeService.deleteIntake(id);
    }

}
