package com.ds.project.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "intake")
public class Intake {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long intakeId;
    private LocalDate startDate;
    private LocalDate endDate;
    private Double dosage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="medicationId")
    private Medication medication;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="patientId")
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Long getIntakeId() {
        return intakeId;
    }

    public void setIntakeId(Long intakeId) {
        this.intakeId = intakeId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getDosage() {
        return dosage;
    }

    public void setDosage(Double dosage) {
        this.dosage = dosage;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }
}
