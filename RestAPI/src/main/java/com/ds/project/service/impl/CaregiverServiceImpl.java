package com.ds.project.service.impl;

import com.ds.project.dto.CaregiverDTO;
import com.ds.project.entity.Caregiver;
import com.ds.project.mapper.CaregiverMapper;
import com.ds.project.repository.CaregiverRepository;
import com.ds.project.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CaregiverServiceImpl implements CaregiverService {

    private final CaregiverMapper caregiverMapper;
    private final CaregiverRepository caregiverRepository;


    @Autowired
    public CaregiverServiceImpl(CaregiverMapper caregiverMapper,
                              CaregiverRepository caregiverRepository) {
        this.caregiverMapper = caregiverMapper;
        this.caregiverRepository = caregiverRepository;
    }

    @Override
    public CaregiverDTO save(CaregiverDTO caregiverDTO) {
        Caregiver caregiver;
        if (caregiverDTO.getId() != null) {
            caregiver = caregiverRepository.findByCaregiverId(caregiverDTO.getId());
        } else {
            caregiver = new Caregiver();
        }
        caregiverMapper.toEntityUpdate(caregiver, caregiverDTO);
        CaregiverDTO caregiverDTO1 = caregiverMapper.toDTO(caregiverRepository.save(caregiver));
        return caregiverDTO1;      }

    @Override
    public List<CaregiverDTO> findAll() {
        return caregiverRepository.findAll()
                .stream()
                .map(caregiverMapper::toDTO)
                .collect(Collectors.toList());     }

    @Override
    public CaregiverDTO findById(Long id) {
        return caregiverMapper.toDTO(caregiverRepository.findByCaregiverId(id));    }


    @Override
    public CaregiverDTO deleteCaregiver(Long id) {
        caregiverRepository.deleteById(id);
        return null;    }
}
