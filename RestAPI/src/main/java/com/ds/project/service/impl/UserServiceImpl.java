package com.ds.project.service.impl;


import com.ds.project.dto.LoginDTO;
import com.ds.project.dto.UserDTO;
import com.ds.project.entity.User;
import com.ds.project.mapper.UserMapper;
import com.ds.project.repository.UserRepository;
import com.ds.project.service.UserService;
import org.hibernate.type.UUIDBinaryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {


    private final UserMapper userMapper;
    private final UserRepository userRepository;


    @Autowired
    public UserServiceImpl( UserMapper userMapper,
            UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public String test() {
        return "text din user service";
    }

    @Override
    public UserDTO deleteUser(UUID id) {
        userRepository.deleteById(id);
        return null;    }

    @Override
    public UserDTO save(UserDTO userDto) {

        User user;
        if (userDto.getUserId() != null) {
            user = userRepository.findByUserId(userDto.getUserId());
        } else {
            //  userDto.setType(T.USER);
            user = new User();
        }
        userMapper.toEntityUpdate(user, userDto);
        UserDTO userDTO1 = userMapper.toDTO(userRepository.save(user));
//        Order order = new Order();
//        order.setUser(user);
//        orderRepository.save(order);
//        System.out.println(order);
        return userDTO1;
    }

    @Override
    public List<UserDTO> findAll() {
        return userRepository.findAll()
                .stream()
                .map(userMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO findByUserId(UUID id) {
//        System.out.printlntinln(userRepository.findByUserId(id).getUsername());
        return userMapper.toDTO(userRepository.findByUserId(id));
    }

    @Override
    public UserDTO login(LoginDTO loginDTO) {
        User user = userRepository.findByUsername(loginDTO.getUsername());

        if (user != null) {
            if (user.getPassword().equals(loginDTO.getPassword())) {
                System.out.println("Parola buna");
                return userMapper.toDTO(user);
            } else {
                System.out.println("Parola gresita");
                return null;
            }

        }
        return null;
    }

    @Override
    public UserDTO register(UserDTO userDto) {
        return null;
    }

    @Override
    public UserDTO changePassword(@NotNull String username, @NotNull String password, @NotNull String newPassword) {
        User user = userRepository.findByUsername(username);

        if (user != null) {
            if (user.getPassword().equals(password)) {
                user.setPassword(newPassword);
            }
        }

        return userMapper.toDTO(userRepository.save(user));
    }

}
