package com.ds.project.service.impl;

import com.ds.project.dto.MedicalRecordDTO;
import com.ds.project.entity.MedicalRecord;
import com.ds.project.mapper.MedicalRecordMapper;
import com.ds.project.mapper.PatientMapper;
import com.ds.project.repository.MedicalRecordRepository;
import com.ds.project.repository.PatientRepository;
import com.ds.project.service.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicalRecordServiceImpl implements MedicalRecordService {

    private final MedicalRecordMapper medicalRecordMapper;
    private final MedicalRecordRepository medicalRecordRepository;


    @Autowired
    public MedicalRecordServiceImpl(MedicalRecordMapper medicalRecordMapper,
                              MedicalRecordRepository medicalRecordRepository) {
        this.medicalRecordMapper = medicalRecordMapper;
        this.medicalRecordRepository = medicalRecordRepository;
    }

    @Override
    public MedicalRecordDTO save(MedicalRecordDTO medicalRecordDTO) {
        MedicalRecord medicalRecord;
        if (medicalRecordDTO.getMedicalRecordId() != null) {
            medicalRecord = medicalRecordRepository.findByMedicalRecordId(medicalRecordDTO.getMedicalRecordId());
        } else {
            medicalRecord = new MedicalRecord();
        }
        medicalRecordMapper.toEntityUpdate(medicalRecord, medicalRecordDTO);
        MedicalRecordDTO medicalRecordDTO1 = medicalRecordMapper.toDTO(medicalRecordRepository.save(medicalRecord));
        return medicalRecordDTO1;       }

    @Override
    public List<MedicalRecordDTO> findAll() {
        return medicalRecordRepository.findAll()
                .stream()
                .map(medicalRecordMapper::toDTO)
                .collect(Collectors.toList());     }

    @Override
    public MedicalRecordDTO findById(Long id) {
        return medicalRecordMapper.toDTO(medicalRecordRepository.findByMedicalRecordId(id));
    }

    @Override
    public MedicalRecordDTO deleteMedicalRecord(Long id) {
        medicalRecordRepository.deleteById(id);
        return null;     }
}
