package com.ds.project.service;

import com.ds.project.dto.MedicationDTO;

import java.util.List;

public interface MedicationService {
    MedicationDTO save(MedicationDTO medicationDTO);

    List<MedicationDTO> findAll();

    MedicationDTO findById(Long id);

    MedicationDTO deleteMedication(Long id);
}
