package com.ds.project.dto;


import com.ds.project.entity.Gender;

import java.time.LocalDate;

public class DoctorDTO {

    private Long doctorId;
    private String name;
    private LocalDate birthDate;
    private String adress;
    private Gender gender;
    private UserDTO user1Dto;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public UserDTO getUser1Dto() {
        return user1Dto;
    }

    public void setUser1Dto(UserDTO user1Dto) {
        this.user1Dto = user1Dto;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }
}
