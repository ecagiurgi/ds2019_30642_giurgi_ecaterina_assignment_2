package com.ds.project.dto;

import com.ds.project.entity.Type;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class LoginDTO {
    private  String username;
    private  String password;
//    @Enumerated(value = EnumType.STRING)
//    private Type type;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public Type getType() {
//        return type;
//    }
//
//    public void setType(Type type) {
//        this.type = type;
//    }
}
