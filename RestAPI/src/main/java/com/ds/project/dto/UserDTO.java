package com.ds.project.dto;


import com.ds.project.entity.Type;
import org.hibernate.type.UUIDBinaryType;

import java.util.UUID;

public class UserDTO {

    private UUID userId;
    private String username;
    private String password;
    private Type type;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
